﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Shielder
{
    public class UserSettings
    {
        private string pathToSettings;
        private string pathToItems;
        private string pathToSprites;

        public UserSettings(string pathToSettings)
        {
            Console.WriteLine(pathToSettings);
            this.pathToSettings = pathToSettings;
            StreamReader reader = new StreamReader(pathToSettings);
            string line = string.Empty;
            string[] split;
            

            using (reader)
            {
                line = reader.ReadLine();
                Console.WriteLine(line);
                split = line.Split(new string[] { " = " }, StringSplitOptions.None);
                pathToItems = split[1];

                line = reader.ReadLine();
                Console.WriteLine(line);
                split = line.Split(new string[] { " = " }, StringSplitOptions.None);
                pathToSprites = split[1];
            }
        }
        //>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>

        public string ItemPath
        {
            get
            {
                return pathToItems;
            }
            set
            {
                pathToItems = value;
            }
        }

        public string SpritePath
        {
            get
            {
                return pathToSprites;
            }
            set
            {
                pathToSprites = value;
            }
        }

        public string SettingsPath
        {
            get
            {
                return pathToSettings;
            }
        }
    }
}
