﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VintageStudios.Planar2D.Shielder;

namespace Shielder
{
    public class Quest : ListViewItem
    {
        private IndexedImage img;
        private QuestInfo info;


        public Quest(QuestInfo info, IndexedImage img)
        {
            this.info = info;
            this.Text = info.Name;
            this.img = img;
        }
        public override string ToString()
        {
            return info.Name;
        }
        public string Print()
        {
            return info.Print();
        }

        //PROPERTIES

        public QuestInfo Info
        {
            get
            {
                return info;
            }
            set
            {
                info = value;
            }
        }
    }
}
