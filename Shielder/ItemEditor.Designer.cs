﻿namespace Shielder
{
    partial class ItemEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbxItem = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numericLevel = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxImage = new System.Windows.Forms.ComboBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radShield = new System.Windows.Forms.RadioButton();
            this.radRing = new System.Windows.Forms.RadioButton();
            this.radNecklace = new System.Windows.Forms.RadioButton();
            this.radGloves = new System.Windows.Forms.RadioButton();
            this.radHelm = new System.Windows.Forms.RadioButton();
            this.radBoots = new System.Windows.Forms.RadioButton();
            this.radArmor = new System.Windows.Forms.RadioButton();
            this.radTwoHand = new System.Windows.Forms.RadioButton();
            this.radOneHand = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.numericAttackSpeed = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericHealth = new System.Windows.Forms.NumericUpDown();
            this.numericDefense = new System.Windows.Forms.NumericUpDown();
            this.numericMinDamage = new System.Windows.Forms.NumericUpDown();
            this.numericMaxDmg = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.radContainer = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxItem)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericLevel)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAttackSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHealth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDefense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMinDamage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxDmg)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbxItem);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(99, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(77, 73);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Preview";
            // 
            // pbxItem
            // 
            this.pbxItem.BackColor = System.Drawing.Color.Transparent;
            this.pbxItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxItem.Location = new System.Drawing.Point(22, 23);
            this.pbxItem.Name = "pbxItem";
            this.pbxItem.Size = new System.Drawing.Size(32, 32);
            this.pbxItem.TabIndex = 1;
            this.pbxItem.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numericLevel);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbxImage);
            this.groupBox2.Controls.Add(this.txtItemName);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(10, 91);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 111);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Properties";
            // 
            // numericLevel
            // 
            this.numericLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericLevel.Location = new System.Drawing.Point(51, 73);
            this.numericLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericLevel.Name = "numericLevel";
            this.numericLevel.Size = new System.Drawing.Size(56, 20);
            this.numericLevel.TabIndex = 5;
            this.numericLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Level:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Image:";
            // 
            // cbxImage
            // 
            this.cbxImage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxImage.FormattingEnabled = true;
            this.cbxImage.Location = new System.Drawing.Point(51, 44);
            this.cbxImage.Name = "cbxImage";
            this.cbxImage.Size = new System.Drawing.Size(186, 21);
            this.cbxImage.TabIndex = 2;
            this.cbxImage.SelectedIndexChanged += new System.EventHandler(this.cbxImage_SelectedIndexChanged);
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(51, 17);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(186, 20);
            this.txtItemName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radContainer);
            this.groupBox3.Controls.Add(this.radShield);
            this.groupBox3.Controls.Add(this.radRing);
            this.groupBox3.Controls.Add(this.radNecklace);
            this.groupBox3.Controls.Add(this.radGloves);
            this.groupBox3.Controls.Add(this.radHelm);
            this.groupBox3.Controls.Add(this.radBoots);
            this.groupBox3.Controls.Add(this.radArmor);
            this.groupBox3.Controls.Add(this.radTwoHand);
            this.groupBox3.Controls.Add(this.radOneHand);
            this.groupBox3.Location = new System.Drawing.Point(12, 208);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(256, 111);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Item Type";
            // 
            // radShield
            // 
            this.radShield.AutoSize = true;
            this.radShield.Location = new System.Drawing.Point(181, 20);
            this.radShield.Name = "radShield";
            this.radShield.Size = new System.Drawing.Size(54, 17);
            this.radShield.TabIndex = 8;
            this.radShield.TabStop = true;
            this.radShield.Text = "Shield";
            this.radShield.UseVisualStyleBackColor = true;
            // 
            // radRing
            // 
            this.radRing.AutoSize = true;
            this.radRing.Location = new System.Drawing.Point(111, 88);
            this.radRing.Name = "radRing";
            this.radRing.Size = new System.Drawing.Size(47, 17);
            this.radRing.TabIndex = 7;
            this.radRing.TabStop = true;
            this.radRing.Text = "Ring";
            this.radRing.UseVisualStyleBackColor = true;
            // 
            // radNecklace
            // 
            this.radNecklace.AutoSize = true;
            this.radNecklace.Location = new System.Drawing.Point(7, 88);
            this.radNecklace.Name = "radNecklace";
            this.radNecklace.Size = new System.Drawing.Size(71, 17);
            this.radNecklace.TabIndex = 6;
            this.radNecklace.TabStop = true;
            this.radNecklace.Text = "Necklace";
            this.radNecklace.UseVisualStyleBackColor = true;
            // 
            // radGloves
            // 
            this.radGloves.AutoSize = true;
            this.radGloves.Location = new System.Drawing.Point(111, 65);
            this.radGloves.Name = "radGloves";
            this.radGloves.Size = new System.Drawing.Size(58, 17);
            this.radGloves.TabIndex = 5;
            this.radGloves.TabStop = true;
            this.radGloves.Text = "Gloves";
            this.radGloves.UseVisualStyleBackColor = true;
            // 
            // radHelm
            // 
            this.radHelm.AutoSize = true;
            this.radHelm.Location = new System.Drawing.Point(111, 42);
            this.radHelm.Name = "radHelm";
            this.radHelm.Size = new System.Drawing.Size(49, 17);
            this.radHelm.TabIndex = 4;
            this.radHelm.TabStop = true;
            this.radHelm.Text = "Helm";
            this.radHelm.UseVisualStyleBackColor = true;
            // 
            // radBoots
            // 
            this.radBoots.AutoSize = true;
            this.radBoots.Location = new System.Drawing.Point(111, 20);
            this.radBoots.Name = "radBoots";
            this.radBoots.Size = new System.Drawing.Size(52, 17);
            this.radBoots.TabIndex = 3;
            this.radBoots.TabStop = true;
            this.radBoots.Text = "Boots";
            this.radBoots.UseVisualStyleBackColor = true;
            // 
            // radArmor
            // 
            this.radArmor.AutoSize = true;
            this.radArmor.Location = new System.Drawing.Point(7, 65);
            this.radArmor.Name = "radArmor";
            this.radArmor.Size = new System.Drawing.Size(52, 17);
            this.radArmor.TabIndex = 2;
            this.radArmor.TabStop = true;
            this.radArmor.Text = "Armor";
            this.radArmor.UseVisualStyleBackColor = true;
            // 
            // radTwoHand
            // 
            this.radTwoHand.AutoSize = true;
            this.radTwoHand.Location = new System.Drawing.Point(7, 42);
            this.radTwoHand.Name = "radTwoHand";
            this.radTwoHand.Size = new System.Drawing.Size(83, 17);
            this.radTwoHand.TabIndex = 1;
            this.radTwoHand.TabStop = true;
            this.radTwoHand.Text = "2H Weapon";
            this.radTwoHand.UseVisualStyleBackColor = true;
            // 
            // radOneHand
            // 
            this.radOneHand.AutoSize = true;
            this.radOneHand.Checked = true;
            this.radOneHand.Location = new System.Drawing.Point(7, 20);
            this.radOneHand.Name = "radOneHand";
            this.radOneHand.Size = new System.Drawing.Size(83, 17);
            this.radOneHand.TabIndex = 0;
            this.radOneHand.TabStop = true;
            this.radOneHand.Text = "1H Weapon";
            this.radOneHand.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.numericAttackSpeed);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.numericHealth);
            this.groupBox4.Controls.Add(this.numericDefense);
            this.groupBox4.Controls.Add(this.numericMinDamage);
            this.groupBox4.Controls.Add(this.numericMaxDmg);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(12, 325);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(256, 210);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Attributes";
            // 
            // label9
            // 
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Location = new System.Drawing.Point(8, 155);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(232, 58);
            this.label9.TabIndex = 10;
            this.label9.Text = "A value of \"0\" means the stat will not be included in game.  Any other value mean" +
                "s it is a buff. ";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // numericAttackSpeed
            // 
            this.numericAttackSpeed.Location = new System.Drawing.Point(92, 109);
            this.numericAttackSpeed.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericAttackSpeed.Name = "numericAttackSpeed";
            this.numericAttackSpeed.Size = new System.Drawing.Size(120, 20);
            this.numericAttackSpeed.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "+AttackSpeed:";
            // 
            // numericHealth
            // 
            this.numericHealth.Location = new System.Drawing.Point(92, 85);
            this.numericHealth.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericHealth.Name = "numericHealth";
            this.numericHealth.Size = new System.Drawing.Size(120, 20);
            this.numericHealth.TabIndex = 7;
            // 
            // numericDefense
            // 
            this.numericDefense.Location = new System.Drawing.Point(92, 63);
            this.numericDefense.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericDefense.Name = "numericDefense";
            this.numericDefense.Size = new System.Drawing.Size(120, 20);
            this.numericDefense.TabIndex = 6;
            // 
            // numericMinDamage
            // 
            this.numericMinDamage.Location = new System.Drawing.Point(92, 40);
            this.numericMinDamage.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericMinDamage.Name = "numericMinDamage";
            this.numericMinDamage.Size = new System.Drawing.Size(120, 20);
            this.numericMinDamage.TabIndex = 5;
            // 
            // numericMaxDmg
            // 
            this.numericMaxDmg.Location = new System.Drawing.Point(92, 18);
            this.numericMaxDmg.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericMaxDmg.Name = "numericMaxDmg";
            this.numericMaxDmg.Size = new System.Drawing.Size(120, 20);
            this.numericMaxDmg.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "+Health:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "+Defense:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "+Min Damage:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "+Max Damage:";
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(112, 541);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save Item";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(193, 541);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // radContainer
            // 
            this.radContainer.AutoSize = true;
            this.radContainer.Location = new System.Drawing.Point(181, 43);
            this.radContainer.Name = "radContainer";
            this.radContainer.Size = new System.Drawing.Size(70, 17);
            this.radContainer.TabIndex = 9;
            this.radContainer.TabStop = true;
            this.radContainer.Text = "Container";
            this.radContainer.UseVisualStyleBackColor = true;
            // 
            // ItemEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 567);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ItemEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item Editor";
            this.Load += new System.EventHandler(this.ItemEditor_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxItem)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericLevel)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAttackSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHealth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDefense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMinDamage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxDmg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbxItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxImage;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radRing;
        private System.Windows.Forms.RadioButton radNecklace;
        private System.Windows.Forms.RadioButton radGloves;
        private System.Windows.Forms.RadioButton radHelm;
        private System.Windows.Forms.RadioButton radBoots;
        private System.Windows.Forms.RadioButton radArmor;
        private System.Windows.Forms.RadioButton radTwoHand;
        private System.Windows.Forms.RadioButton radOneHand;
        private System.Windows.Forms.NumericUpDown numericLevel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radShield;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericAttackSpeed;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericHealth;
        private System.Windows.Forms.NumericUpDown numericDefense;
        private System.Windows.Forms.NumericUpDown numericMinDamage;
        private System.Windows.Forms.NumericUpDown numericMaxDmg;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RadioButton radContainer;
    }
}