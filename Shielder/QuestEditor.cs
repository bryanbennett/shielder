﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Shielder.Properties;
using VintageStudios.Planar2D.Shielder;

namespace Shielder
{
    public partial class QuestEditor : Form
    {
        private Main main;
        private IndexedImage img;
        private Quest activeQuest;
        private ListView listViewMonster;
        private ListView listViewItem;
        private bool overwrite = false;

        public QuestEditor(Main main)
        {
            InitializeComponent();
            this.main = main;
            Image temp = (Image)Resources.ResourceManager.GetObject("Parchment.png");
            img = new IndexedImage(temp, "Parchment.png");
            Init();
        }
        public QuestEditor(Quest quest, Main main)
        {
            InitializeComponent();
            Image temp = (Image)Resources.ResourceManager.GetObject("Parchment.png");
            img = new IndexedImage(temp, "Parchment.png");
            this.activeQuest = quest;
            this.main = main;

            Init();

            activeQuest = quest;
            txtName.Text = quest.Info.Name;
            numericMinLevel.Value = quest.Info.MinLevel;
            rtbPrompt.Text = quest.Info.Prompt;
            numericGoldReward.Value = quest.Info.RewardGold;
            numericXPReward.Value = quest.Info.RewardExperience;

            if (!string.IsNullOrEmpty(quest.Info.RewardItem))
            {
                int index = cbxItemReward.Items.IndexOf(quest.Info.RewardItem);
                cbxItemReward.SelectedIndex = index;
            }

            if (quest.Info.KillCondition)
            {
                radKill.Checked = true;
                int index = cbxTargetMonster.Items.IndexOf(quest.Info.TargetMonster);
                cbxTargetMonster.SelectedIndex = index;
            }
            if (quest.Info.CollectCondition)
            {
                radCollect.Checked = true;
                int index = cbxTargetItem.Items.IndexOf(quest.Info.TargetItem);
                cbxTargetItem.SelectedIndex = index;
            }
            overwrite = true;
        }
        private void Init()
        {
            listViewItem = main.getItemListView();
            listViewMonster = main.getMonsterListView();

            foreach (Monster entry in listViewMonster.Items)
            {
                cbxTargetMonster.Items.Add(entry.MonsterInfo.MonsterName);
            }
            foreach (Item entry in listViewItem.Items)
            {
                cbxTargetItem.Items.Add(entry.ItemInfo.Name);
                cbxItemReward.Items.Add(entry.ItemInfo.Name);
            }

        }
        private void QuestEditor_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            QuestInfo q = new QuestInfo(txtName.Text);

            q.MinLevel = numericMinLevel.Value;
            q.Prompt = rtbPrompt.Text;
            q.RewardItem = (string)cbxItemReward.SelectedItem;
            q.RewardGold = numericGoldReward.Value;
            q.RewardExperience = numericXPReward.Value;
            q.KillCondition = radKill.Checked;
            q.CollectCondition = radCollect.Checked;

            if (radKill.Checked)
            {
                q.TargetMonster = (string)cbxTargetMonster.SelectedItem;
            }
            if (radCollect.Checked)
            {
                q.TargetItem = (string)cbxTargetItem.SelectedItem;
            }

            Quest quest = new Quest(q, img);
            if (overwrite)
            {
                main.OverWriteQuest(quest);
            }
            else
            {
                main.AddToListView(quest);
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radKill_CheckedChanged(object sender, EventArgs e)
        {
            if (radKill.Checked)
            {
                cbxTargetMonster.Enabled = true;
                cbxTargetItem.Enabled = false;
            }
        }

        private void radCollect_CheckedChanged(object sender, EventArgs e)
        {
            if (radCollect.Checked)
            {
                cbxTargetMonster.Enabled = false;
                cbxTargetItem.Enabled = true;
            }
        }
    }
}
