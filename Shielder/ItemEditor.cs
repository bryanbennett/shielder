﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VintageStudios.Planar2D.Shielder;

namespace Shielder
{
    public partial class ItemEditor : Form
    {
        private Main main;
        private Dictionary<string, IndexedImage> images;
        private bool overwrite = false;
        public ItemEditor(Main main)
        {
            InitializeComponent();
            this.main = main;
            init();
        }
        public ItemEditor(Item obj, Main main)
        {
            InitializeComponent();
            this.main = main;
            init();
            overwrite = true;
            ItemInfo item = obj.ItemInfo;
            txtItemName.Text = item.Name;
            int index = cbxImage.Items.IndexOf(item.ImageName);
            cbxImage.SelectedIndex = index;
            numericLevel.Value = item.Level;

            string type = item.Type.ToString();
            if (type.Equals("Weapon_1H"))
            {
                radOneHand.Checked = true;
            }
            if (type.Equals("Weapon_2H"))
            {
                radTwoHand.Checked = true;
            }
            if (type.Equals("Armor"))
            {
                radArmor.Checked = true;
            }
            if (type.Equals("Necklace"))
            {
                radNecklace.Checked = true;
            }
            if (type.Equals("Boots"))
            {
                radBoots.Checked = true;
            }
            if (type.Equals("Helm"))
            {
                radHelm.Checked = true;
            }
            if (type.Equals("Gloves"))
            {
                radGloves.Checked = true;
            }
            if (type.Equals("Ring"))
            {
                radRing.Checked = true;
            }
            if (type.Equals("Shield"))
            {
                radShield.Checked = true;
            }
            if (type.Equals("Container"))
            {
                radContainer.Checked = true;
            }

            numericMaxDmg.Value = item.MaxDamage;
            numericMinDamage.Value = item.MinDamage;
            numericDefense.Value = item.Defense;
            numericHealth.Value = item.Health;
            numericAttackSpeed.Value = item.AttackSpeed;
        }
        private void init()
        {
            this.images = main.getImageList();
            foreach (KeyValuePair<string, IndexedImage> entry in images)
            {
                if (entry.Key == null)
                {
                    MessageBox.Show("yup");
                }
                cbxImage.Items.Add(entry.Key);
            }
        }

        private void cbxImage_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = cbxImage.SelectedItem.ToString();
            pbxItem.Image = images[name].Image;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string name = cbxImage.SelectedItem.ToString();
            IndexedImage img = images[name];
            ItemInfo i = new ItemInfo(txtItemName.Text, img.ImageName);
            string type = "Weapon_1H";
            i.Level = numericLevel.Value;
            //item type logic
            if (radOneHand.Checked)
            {
                //nothing
            }
            if (radTwoHand.Checked)
            {
                type = "Weapon_2H";
            }
            if (radArmor.Checked)
            {
                type = "Armor";
            }
            if (radNecklace.Checked)
            {
                type = "Necklace";
            }
            if (radBoots.Checked)
            {
                type = "Boots";
            }
            if (radHelm.Checked)
            {
                type = "Helm";
            }
            if (radGloves.Checked)
            {
                type = "Gloves";
            }
            if (radRing.Checked)
            {
                type = "Ring";
            }
            if (radShield.Checked)
            {
                type = "Shield";
            }
            if (radContainer.Checked)
            {
                type = "Container";
            }
            i.Type = (ItemType)Enum.Parse(typeof(ItemType), type);

            i.MaxDamage = numericMaxDmg.Value;
            i.MinDamage = numericMinDamage.Value;

            if (i.MaxDamage > 0 && i.MaxDamage < i.MinDamage)
            {
                MessageBox.Show("Minimum damage must never be higher than maximum damage unless maximum damage is set to 0.", "Min Damage > Max Damage");
                return;
            }

            i.Defense = numericDefense.Value;
            i.Health = numericHealth.Value;
            i.AttackSpeed = numericAttackSpeed.Value;
            Item temp = new Item(i, img);
            if (overwrite)
            {
                main.OverwriteItem(temp);
            }
            else
            {
                main.AddToListView(temp);
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ItemEditor_Load(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
    }
}
