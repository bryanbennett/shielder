﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VintageStudios.Planar2D.Shielder;

namespace Shielder
{
    public partial class MonsterEditor : Form
    {
        private Main main;
        private Monster activeMonster;
        private Dictionary<string, IndexedImage> images;
        private ListView listViewMonster;
        private ListView listViewItem;
        private IndexedImage selected;
        private bool overwrite = false;
        public MonsterEditor(Main main)
        {
            InitializeComponent();
            this.main = main;
            init();
        }
        public MonsterEditor(Main main, Monster activeMonster)
        {
            InitializeComponent();
            this.main = main;
            this.activeMonster = activeMonster;
            init();
            MonsterInfo monster = activeMonster.MonsterInfo;
            txtMonsterName.Text = monster.MonsterName;
            int index = cbxSpritesheet.Items.IndexOf(monster.ImageName);
            cbxSpritesheet.SelectedIndex = index;
            numericHealth.Value = monster.Health;
            numericMaxDamage.Value = monster.MaxDamage;
            numericMinDamage.Value = monster.MinimumDamage;
            numericLevel.Value = monster.Level;
            numericSight.Value = monster.Sight;
            numericAtkRange.Value = monster.AttackRange;
            numericMonsterAttackSpeed.Value = monster.AttackSpeed;
            numericMovtSpeed.Value = monster.MovementSpeed;
            numericExperience.Value = monster.Experience;
            numericMonsterGold.Value = monster.Gold;
            chkHostile.Checked = monster.Hostile;
            chkFlying.Checked = monster.Flying;
            chkInvincible.Checked = monster.Invincible;
            chkNPC.Checked = monster.NPC;
            chkStoic.Checked = monster.Stoic;
            chkSummoner.Checked = monster.Summoner;
            if (monster.Summoner)
            {
                index = cbxFamiliar.Items.IndexOf(monster.Familiar);
                cbxFamiliar.SelectedIndex = index;
            }
            chkItemDropper.Checked = monster.ItemDropper;
            if (monster.ItemDropper)
            {
                index = cbxItemDropped.Items.IndexOf(monster.DroppedItem);
                cbxItemDropped.SelectedIndex = index;
            }
            numericDropChance.Value = monster.DropChance;
            chkPoisonous.Checked = monster.Poisonous;
            numericDmgPerTick.Value = monster.DamagePerTick;
            numericTicksPerSecond.Value = monster.TicksPerSecond;

            chkNightCrawler.Checked = monster.NightCrawler;
            if (monster.NightCrawler)
            {
                numericStartTime.Value = monster.StartTime;
                numericEndTime.Value = monster.EndTime;
            }

            overwrite = true;
        }
        private void init()
        {
            listViewMonster = main.getMonsterListView();
            listViewItem = main.getItemListView();
            this.images = main.getSpriteSheetList();
            foreach(KeyValuePair<string, IndexedImage> entry in images)
            {
                cbxSpritesheet.Items.Add(entry.Key);
            }
            foreach (Monster entry in listViewMonster.Items)
            {
                cbxFamiliar.Items.Add(entry.MonsterInfo.MonsterName);
            }
            foreach (Item entry in listViewItem.Items)
            {
                cbxItemDropped.Items.Add(entry.ItemInfo.Name);
            }
        }
        private void chkSummoner_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSummoner.Checked)
            {
                cbxFamiliar.Enabled = true;
            }
            else
            {
                cbxFamiliar.Enabled = false;
            }
        }

        private void chkItemDropper_CheckedChanged(object sender, EventArgs e)
        {
            if (chkItemDropper.Checked)
            {
                cbxItemDropped.Enabled = true;
                numericDropChance.Enabled = true;
            }
            else
            {
                cbxItemDropped.Enabled = false;
                numericDropChance.Enabled = false;
            }
        }

        private void chkPoisonous_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPoisonous.Checked)
            {
                numericDmgPerTick.Enabled = true;
                numericTicksPerSecond.Enabled = true;
                chkPoisonStackable.Enabled = true;
            }
            else
            {
                numericDmgPerTick.Enabled = false;
                numericTicksPerSecond.Enabled = false;
                chkPoisonStackable.Enabled = false;
            }
        }

        private void btnSaveMob_Click(object sender, EventArgs e)
        {
            //input verification
            if (txtMonsterName.Text.Equals(""))
            {
                MessageBox.Show("Input valid name for monster.", "Input Name");
                return;
            }
            if (cbxSpritesheet.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a spritesheet for this monster.", "Select Spritesheet");
                return;
            }
            if (numericMaxDamage.Value < numericMinDamage.Value)
            {
                MessageBox.Show("The maximum damage must be greater than or equal to the minimum damage.", "Crazy Math");
                return;
            }
            if (chkSummoner.Checked && cbxFamiliar.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a familiar or uncheck the Summoner check box.", "A Summoner Needs Something To Summon");
                return;
            }
            if (chkItemDropper.Checked && cbxItemDropped.SelectedIndex < 0)
            {
                MessageBox.Show("Please select an item to be dropped by the poor sap the player will kill.", "Select Dropped Item");
                return;
            }
            //Monster m = new Monster(txtMonsterName.Text, selected);
            MonsterInfo m = new MonsterInfo(txtMonsterName.Text, selected.ImageName);
            m.Health = numericHealth.Value;
            m.MaxDamage = numericMaxDamage.Value;
            m.MinimumDamage = numericMinDamage.Value;
            m.AttackSpeed = numericMonsterAttackSpeed.Value;
            m.MovementSpeed = numericMovtSpeed.Value;
            m.Experience = numericExperience.Value;
            m.Gold = numericMonsterGold.Value;
            m.Level = numericLevel.Value;
            m.Sight = numericSight.Value;
            m.AttackRange = numericAtkRange.Value;
            m.Hostile = chkHostile.Checked;
            m.Flying = chkFlying.Checked;
            m.Invincible = chkInvincible.Checked;
            m.Summoner = chkSummoner.Checked;
            m.NightCrawler = chkNightCrawler.Checked;
            m.Stoic = chkStoic.Checked;
            m.NPC = chkNPC.Checked;

            if (m.Summoner)
            {
                m.Familiar = (string)cbxFamiliar.SelectedItem;
            }

            m.ItemDropper = chkItemDropper.Checked;
            if (m.ItemDropper)
            {
                m.DroppedItem = (string)cbxItemDropped.SelectedItem;
                m.DropChance = numericDropChance.Value;
            }

            m.Poisonous = chkPoisonous.Checked;
            if (m.Poisonous)
            {
                m.DamagePerTick = numericDmgPerTick.Value;
                m.TicksPerSecond = numericTicksPerSecond.Value;
            }
            if (m.NightCrawler)
            {
                m.StartTime = numericStartTime.Value;
                m.EndTime = numericEndTime.Value;
            }

            Monster monster = new Monster(m, selected);
            if (overwrite)
            {
                main.OverwriteMob(monster);
            }
            else
            {
                main.AddToListView(monster);
            }
            this.Close();
        }

        private void btnMonsterCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void cbxSpritesheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = cbxSpritesheet.SelectedItem.ToString();
            selected = images[name];
            pbxMonster.Image = selected.Image;
        }

        private void MonsterEditor_Load(object sender, EventArgs e)
        {

        }

        private void chkNightCrawler_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNightCrawler.Checked)
            {
                numericStartTime.Enabled = true;
                numericEndTime.Enabled = true;
            }
            else
            {
                numericStartTime.Enabled = false;
                numericEndTime.Enabled = false;
            }
        }
    }
}
