﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Shielder
{
    /// <summary>
    /// Class used in conjunction with a listViewItem.
    /// </summary>
    public class IndexedImage
    {
        private Image img;
        private string imgName;

        /// <summary>
        /// Returns the image associated with this indexed image.
        /// </summary>
        public Image Image
        {
            get
            {
                return img;
            }
        }
        /// <summary>
        /// Returns the name of the image.
        /// </summary>
        public string ImageName
        {
            get
            {
                return imgName;
            }
        }

        public IndexedImage(Image img, string imgName)
        {
            this.img = img;
            this.imgName = imgName;
        }

    }
}
